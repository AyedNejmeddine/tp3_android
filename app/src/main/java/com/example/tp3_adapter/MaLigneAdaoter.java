package com.example.tp3_adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class MaLigneAdaoter extends ArrayAdapter<String> {
    Activity context;
    String [] items;

    MaLigneAdaoter(Activity context, String [] items){
        super(context, R.layout.ma_ligne, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View ligne = inflater.inflate(R.layout.ma_ligne, null);
        ImageView icon = (ImageView) ligne.findViewById(R.id.image);
        TextView label = (TextView) ligne.findViewById(R.id.note);

        float note = Float.valueOf(items[position]);
        label.setText(String.valueOf(note));
        icon.setImageResource(
                (note>=10)? R.drawable.ressir : R.drawable.echek
        );
        return ligne;
    }
}
