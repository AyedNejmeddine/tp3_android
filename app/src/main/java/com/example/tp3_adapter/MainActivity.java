package com.example.tp3_adapter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ListView notesList;
    AutoCompleteTextView matieresTV;
    String [] mesNotes = {"12.5", "4.75", "15", "10.25", "7.5", "16.75"};
    String [] mesMatieres = {"Matiere1", "Matiere2", "Matiere3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notesList = (ListView) findViewById(R.id.listview);
        /*ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mesNotes);
        notesList.setAdapter(listAdapter);*/

        matieresTV = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        matieresTV.setAdapter(new ArrayAdapter<String>(
                this, android.R.layout.simple_dropdown_item_1line, mesMatieres
        ));

        /*notesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView v = (TextView) view;
                float n = Float.valueOf((String) v.getText());
                if (n>=10) Toast.makeText(MainActivity.this, "Reussi !", Toast.LENGTH_SHORT).show();
                else Toast.makeText(MainActivity.this, "Echec !", Toast.LENGTH_SHORT).show();
            }
        });*/

        matieresTV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String [] tab = {mesNotes[position], mesNotes[position+1]};
                ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, tab);
                notesList.setAdapter(new MaLigneAdaoter(MainActivity.this, tab));

                notesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        float n = Float.valueOf((String) tab[position]);
                        if (n>=10) Toast.makeText(MainActivity.this, "Reussi !", Toast.LENGTH_SHORT).show();
                        else Toast.makeText(MainActivity.this, "Echec !", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}